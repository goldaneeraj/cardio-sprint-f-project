/*********************************
CORE PACKAGES
**********************************/
var q = require('q'),
    // async = require('async'),
    // _ = require('underscore'),
    // nodemailer = require('nodemailer'),
    // fs = require('fs'),
    express = require('express'),
    session = require('express-session'),
    router = express.Router();

/*********************************
MODULE PACKAGES
**********************************/
var config = require('./config');

/*********************************
Module Package
**********************************/
var MyModule = function () {

	const authenticateJWT = (req, res, next) => {
	    const authHeader = req.headers.authorization;
	    if (authHeader) {
	        const token = authHeader.split(' ')[1];
	        jwt.verify(token, config.jwt_secret, (err, user) => {
	            if (err) {
	            	console.log(err);
	                return res.sendStatus(403);
	            }else{
	            	if(user && user.username){
		            	db.query('SELECT * FROM tbl_users WHERE (Email="'+user.username+'" OR User_Name="'+user.username+'") AND status = 3', function (error, results, fields) {
							if (error){
								console.log(error);
								return res.sendStatus(403);
							}else{
								if(results && results[0]){
									// console.log("user",results[0]);
						            req.user = results[0];
									req.session.user = results[0];
						            next();
						        }else{
						        	return res.sendStatus(403);
						        }
							}
						});
	            	}else{
	            		return res.sendStatus(403);
	            	}
	            }
	            
	        });
	    } else {
	        res.sendStatus(401);
	    }
	};

    return {
        authenticateJWT:authenticateJWT
    }
}();

module.exports = MyModule;
