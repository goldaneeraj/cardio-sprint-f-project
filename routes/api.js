/*********************************
CORE PACKAGES
**********************************/
var express = require('express');
var router = express.Router();

/*********************************
MODULE PACKAGES
**********************************/
var config = require('../config');
var db = require('../dbConnection');
var controller = require('../controllers/userController.js');
const auth = require('../authentication');
// ssh -i neudigiKey.pem ubuntu@3.109.183.25

/*********************************
GET REQUESTS
**********************************/ 

router.get('/:id', (req, res) => {
	controller.getUser(req, res);
});

// router.get('/sendOtp', (req, res) => {
// 	controller.sendOtp(req, res);
// });

// router.get('/verifyOtp', (req, res) => {
// 	controller.verifyOtp(req, res);
// });

/*********************************
POST REQUESTS
**********************************/

router.post('/loginPass', (req, res) => {
	controller.loginPass(req, res);
});

router.post('/userRegistration', (req, res) => {
	controller.userRegistration(req, res);
});

router.post('/userLogin', (req, res) => {
	controller.userLogin(req, res);
});

router.post('/forgotPassword', (req, res) => {
	controller.forgotPassword(req, res);
});

router.post('/editProfile',(req, res) => {
// router.post('/editProfile',auth.validateToken,(req, res) => {
	controller.editProfile(req, res);
});

router.post('/updateUserStatus',(req, res) => {
	controller.updateUserStatus(req, res);
});

router.post('/changePassword',auth.validateToken,(req, res) => {
	controller.changePassword(req, res);
});

module.exports = router;