var express = require('express');
var router = express.Router();
var config = require('../config');
var db = require('../dbConnection');
 
/*********************************
GET REQUESTS
**********************************/
router.get('/', function(req, res, next) {
    if (req.session.user) {
        res.render('demographics/list', {
            "username": req.session.user,
            "title": "Dashboard",
            "menu": "",
            "result":[]
        });

    } else {
        res.redirect("/");
    }
});

router.post('/gridData', function(req, res, next) {
    // console.log("------------hiii-------------");
    var query = {};
    var limit = req.body.limit ? parseInt(req.body.limit) : 50;
    var search_by = req.body.search_by ? req.body.search_by : "";
    var sort_by = req.body.sort_by ? req.body.sort_by : "id";
    var order =  "desc";
    var page = req.body.page ? parseInt(req.body.page) : 0;
    var columns = req.body.columns ? req.body.columns : [];
    var filter_columns = {};
    var draw = req.body.draw ? parseInt(req.body.draw) : 1;
    var start = req.body.start ? parseInt(req.body.start) : 0;
    if(req.body.search_query){
        var search_query = req.body.search_query;
    }
    if(search_by && search_by!=""){
        search_by = " ( LOWER(first_name) LIKE '%"+search_by+"%' OR LOWER(last_name) LIKE '%"+search_by+"%' OR reg_phone_number LIKE '%"+search_by+"%' OR LOWER(aadhar_number) LIKE '%"+search_by+"%' ) AND ";
    }else{
        search_by = "";
    }
    // query.branch = search_query.branch; }
    const sql = "SELECT * FROM `cs_demographics` WHERE "+ search_by +" status = 1 order by "+sort_by+" "+order+" limit "+ start +","+limit;
    console.log(sql);
    db.query({ sql }, function (error, users, fields) {
        if(users){
            res.json({
                count: users.length,
                result: users,
                limit: limit,
                start: limit * page,
                draw: draw, 
                page: page
            });
        }else{
            res.json({
                count: 0,
                result: [],
                limit: limit,
                start: limit * page,
                draw: draw, 
                page: page
            });
        }
    });
});

module.exports = router;
