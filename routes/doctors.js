var express = require('express');
var router = express.Router();
var config = require('../config');
var db = require('../dbConnection');
var async = require("async");
 
/*********************************
GET REQUESTS
**********************************/
router.get('/', function(req, res, next) {
    if (req.session.user) {
        res.render('doctors/list', {
            "username": req.session.user,
            "title": "Dashboard",
            "menu": "",
            "result":[]
        });

    } else {
        res.redirect("/");
    }
});

router.get('/form', function(req, res, next) {
    if (req.session.user) {
        res.render('doctors/form', {
            "username": req.session.user,
            "title": "Doctor Form",
            "menu": "",
            "result":[]
        });
    } else {
        res.redirect("/");
    }
});


router.get('/edit/:uId', function(req, res, next) {
    if (req.session.user) {
        let uId = req.params.uId;
        const sql = "SELECT * FROM `user` where uType = 'D' AND uId = '"+uId+"'";
        db.query({ sql }, function (error, users, fields) {
            if (users && users[0]) {
                res.render('doctors/edit', {
                    "username": req.session.user,
                    "title": "Doctor Form",
                    "menu": "",
                    "editUser":users[0],
                    "result":[]
                });
            }
        });
    } else {
        res.redirect("/");
    }
});


router.post('/gridData', function(req, res, next) {
    // console.log("------------hiii-------------");
    var query = {};
    var limit = req.body.limit ? parseInt(req.body.limit) : 50;
    var search_by = req.body.search_by ? req.body.search_by : "";
    var sort_by = req.body.sort_by ? req.body.sort_by : "uId";
    var order =  "desc";
    var page = req.body.page ? parseInt(req.body.page) : 0;
    var start = page*limit;
    // var columns = req.body.columns ? req.body.columns : [];
    
    if(req.body.search_query){
        var search_query = req.body.search_query;
    }
    // if(search_by && search_by!=""){
    //     search_by = " ( LOWER(uName) LIKE '%"+search_by+"%' OR LOWER(uEmail) LIKE '%"+search_by+"%' OR uMobile LIKE '%"+search_by+"%' OR LOWER(uLocation) LIKE '%"+search_by+"%' OR LOWER(uBrandName) LIKE '%"+search_by+"%' ) AND ";
    // }else{
    //     search_by = "";
    // }
    // query.branch = search_query.branch; }
    
    async.parallel([
        function(callback) {
            const sql = "SELECT * FROM `user` WHERE uType = 'D' AND status != 0 order by "+sort_by+" "+order+" limit "+ start +","+limit;
            console.log(sql);
            db.query({ sql }, function (error, users, fields) {
                if(users){
                    callback(null, users);
                }else{
                    callback(null, []);   
                }
            });
        },
        function(callback) {
            const sql = "SELECT count(*) as count FROM `user` WHERE uType = 'D' AND status != 0 ";
            console.log(sql);
            db.query({ sql }, function (error, users, fields) {
                if(users){
                    if(users && users.length && users[0]['count']){
                        callback(null, users[0]['count']);
                    }else{
                        callback(null, 0);
                    }
                }else{
                    callback(null, 0);   
                }
            });
        }],
    function(err, results) {
        // the results array will equal [1, 2] even though
        res.json({
            count: results[1],
            result: results[0],
            limit: limit,
            start: limit * page,
            page: page
        });
    });
});


router.post('/gridData', function(req, res, next) {
    // console.log("------------hiii-------------");
    var query = {};
    var limit = req.body.limit ? parseInt(req.body.limit) : 50;
    var search_by = req.body.search_by ? req.body.search_by : "";
    var sort_by = req.body.sort_by ? req.body.sort_by : "uId";
    var order =  "desc";
    var page = req.body.page ? parseInt(req.body.page) : 0;
    var columns = req.body.columns ? req.body.columns : [];
    var filter_columns = {};
    var draw = req.body.draw ? parseInt(req.body.draw) : 1;
    var start = req.body.start ? parseInt(req.body.start) : 0;
    if(req.body.search_query){
        var search_query = req.body.search_query;
    }
    if(search_by && search_by!=""){
        search_by = " ( LOWER(uName) LIKE '%"+search_by+"%' OR LOWER(uEmail) LIKE '%"+search_by+"%' OR uMobile LIKE '%"+search_by+"%' OR LOWER(uLocation) LIKE '%"+search_by+"%' OR LOWER(uBrandName) LIKE '%"+search_by+"%' ) AND ";
    }else{
        search_by = "";
    }
    // query.branch = search_query.branch; }
    const sql = "SELECT * FROM `user` WHERE "+ search_by +" uType = 'D' AND status != 0 order by "+sort_by+" "+order+" limit "+ start +","+limit;
    console.log(sql);
    db.query({ sql }, function (error, users, fields) {
        if(users){
            res.json({
                count: users.length,
                result: users,
                limit: limit,
                start: limit * page,
                draw: draw, 
                page: page
            });
        }else{
            res.sendStatus(401);
        }
    });
});

module.exports = router;
