
/*********************************
CORE PACKAGES
**********************************/
var express = require('express');
var router = express.Router();
var q = require('q');
var fs = require('fs');
var async = require('async');
var request = require('request');
var busboy = require('connect-busboy');
var config = require('../config.js');
var Imagemin = require('imagemin');
require('dotenv').config()
const path = require('path');
var Promise = require('promise');
var unzip = require('unzipper');

router.use(busboy());
var bucket_Url = "/";
var local_bucket_path = "/";

router.post('/upload', function (req, res) {
    var hostname = req.headers.host;
    req.pipe(req.busboy);
    var fields = {};
    req.busboy.on('field', function (key, value, keyTruncated, valueTruncated) {
        fields[key] = value;
    });
    req.busboy.on('file', function (fieldname, file, filename) {
        var filename_new = filename.replace(".zip", "");
        filename_new = filename_new.replace(".ZIP", "");
        filename_new = filename_new.replace(" (1)", "");
        filename_new = filename_new.replace("(1)", "");
        filename = filename.replace(/\s/g, '');
        let fileExtn = filename.split(".").slice(-1)[0] ;
        var today = new Date();
        
        var myFileName = (today.getMonth() + 1) + '-' + today.getDate() + '-' + today.getFullYear() + '_' + today.getHours() + '-' + today.getMinutes();
        filename = myFileName + '_' + filename;

        console.log("==============>",fields);
        if(fields['fileName'] && fields['fileName']!=""){
            filename = fields['fileName'] + '.' + fileExtn;
        }
        // fs.chmodSync("public/uploads", 0777);
        var local_file_path = "public/uploads/" + fields['path'] + '/';
        // var local_file_path = fields['path'] + '/';
        if (!fs.existsSync(local_file_path)) {
            fs.mkdirSync(local_file_path);
        }
        // fs.chmodSync(local_file_path, 0777);
        
        var fstream = fs.createWriteStream(local_file_path + filename);
        file.pipe(fstream);
        fstream.on('close', function (err, result) {
            var destPath = '/uploads/'+fields['path'];
            res.json({
                message: 'success',
                url: '/uploads/'+fields['path'] + '/' + filename,
                folderName: destPath
            });
        });
    });
});

module.exports = router;
