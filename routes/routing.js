/*********************************
CORE PACKAGES
**********************************/
var express = require('express');
// const debug = require('debug')('ERP:server');
var router = express.Router();

/*********************************
MODULE PACKAGES
**********************************/
var doctors = require('../routes/doctors.js');
router.use('/doctors', doctors);

var demographics = require('../routes/demographics.js');
router.use('/demographics', demographics);

var api = require('../routes/api.js');
router.use('/api', api);

var upload = require('../routes/upload.js');
router.use('/upload', upload);

module.exports = router;
