-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2022 at 07:54 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `cp_demographics`
--

CREATE TABLE `cp_demographics` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `sex` varchar(12) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `reg_phone_number` varchar(13) DEFAULT NULL,
  `aadhar_number` varchar(12) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `last_visit` date DEFAULT NULL,
  `next_visit` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(1) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cp_diagnosis`
--

CREATE TABLE `cp_diagnosis` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `diagnosis_code` varchar(20) NOT NULL,
  `diagnosis_code_type` varchar(30) DEFAULT NULL,
  `diagnosis_description` text DEFAULT NULL,
  `diagnosis_date` date DEFAULT NULL,
  `diagnosis_type_category` varchar(30) DEFAULT NULL,
  `resolution_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cp_ecg`
--

CREATE TABLE `cp_ecg` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `ecg_performed_date` date DEFAULT NULL,
  `ecg_indication` text DEFAULT NULL,
  `ecg_interpretation` text DEFAULT NULL,
  `ecg_upload` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cp_echo`
--

CREATE TABLE `cp_echo` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `echo_performed_date` date DEFAULT NULL,
  `echo_indication` text DEFAULT NULL,
  `echo_interpretation` text DEFAULT NULL,
  `echo_image_upload` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cp_labs`
--

CREATE TABLE `cp_labs` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `lab_ordered_id` varchar(20) NOT NULL,
  `lab_ordered_date` date DEFAULT NULL,
  `lab_description` text DEFAULT NULL,
  `lab_result` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cp_medication`
--

CREATE TABLE `cp_medication` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `medication_code` varchar(20) NOT NULL,
  `medication_code_type` varchar(30) DEFAULT NULL,
  `medication_description` text DEFAULT NULL,
  `medication_start_date` date DEFAULT NULL,
  `medication_end_date` date DEFAULT NULL,
  `reason_for_discontinuation` text DEFAULT NULL,
  `patient_report` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cp_procedure`
--

CREATE TABLE `cp_procedure` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(20) NOT NULL,
  `procedure_code` varchar(20) NOT NULL,
  `procedure_code_type` varchar(30) DEFAULT NULL,
  `procedure_description` text DEFAULT NULL,
  `procedure_date` date DEFAULT NULL,
  `procedure_status` varchar(1) NOT NULL DEFAULT 'y',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cp_procedure`
--

INSERT INTO `cp_procedure` (`id`, `patient_id`, `procedure_code`, `procedure_code_type`, `procedure_description`, `procedure_date`, `procedure_status`, `created_by`, `created_at`, `status`) VALUES
(1, '1234567890', 'G1234', 'CPT', 'Excision of Polyp', '1988-11-12', 'C', 1, '2022-10-23 22:51:41', 'X'),
(2, '1234567890', 'G1234', 'CPT', 'Excision of Polyp', '1988-11-12', 'C', 1, '2022-10-23 22:51:55', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uId` int(11) NOT NULL,
  `uName` varchar(500) NOT NULL,
  `uEmail` varchar(500) NOT NULL,
  `uCity` varchar(500) NOT NULL,
  `uState` varchar(500) NOT NULL,
  `uLocation` varchar(100) DEFAULT NULL,
  `uMobile` varchar(500) NOT NULL,
  `uPassword` varchar(500) NOT NULL,
  `userUid` varchar(20) NOT NULL,
  `uType` varchar(1) NOT NULL DEFAULT 'D',
  `uBrandName` varchar(150) DEFAULT NULL,
  `uBrandLogo` varchar(200) DEFAULT NULL,
  `uCategory` varchar(100) DEFAULT NULL,
  `uSpeciality` text DEFAULT NULL,
  `uCreatedBy` int(11) NOT NULL,
  `uUpdatedBy` int(11) NOT NULL,
  `uCreatedDate` date NOT NULL,
  `uUpdatedDate` date NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uId`, `uName`, `uEmail`, `uCity`, `uState`, `uLocation`, `uMobile`, `uPassword`, `userUid`, `uType`, `uBrandName`, `uBrandLogo`, `uCategory`, `uSpeciality`, `uCreatedBy`, `uUpdatedBy`, `uCreatedDate`, `uUpdatedDate`, `status`) VALUES
(1, 'Admin', 'admin@admin.com', 'Mumbai', 'Maharashtra', NULL, '8087307658', 'U2FsdGVkX18NgEPAoBPhqTmPxRODoyfYfk832QH321E=', 'MsD7borqSp', 'A', '', NULL, NULL, NULL, 0, 0, '2020-11-08', '2020-11-08', '1'),
(5, 'Doctor One', 'doctor1@admin.com', '', '', NULL, '9876543210', 'U2FsdGVkX1/B5rTXQzA7WlFJCA0yNE4WRe3Y+1SaEyA=', '5Nv00DDrsb', 'D', NULL, NULL, NULL, NULL, 0, 0, '2022-12-02', '2022-12-02', '1'),
(7, 'Doctor Two', 'doctor2@admin.com', '', '', NULL, '9876543210', 'U2FsdGVkX19dBWbhnC4wjZwNGite2gDuXrJpRveVzt0=', 'kmed3pii2e', 'D', NULL, NULL, NULL, NULL, 0, 0, '2022-12-02', '2022-12-02', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cp_demographics`
--
ALTER TABLE `cp_demographics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_diagnosis`
--
ALTER TABLE `cp_diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_ecg`
--
ALTER TABLE `cp_ecg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_echo`
--
ALTER TABLE `cp_echo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_labs`
--
ALTER TABLE `cp_labs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_medication`
--
ALTER TABLE `cp_medication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_procedure`
--
ALTER TABLE `cp_procedure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cp_demographics`
--
ALTER TABLE `cp_demographics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_diagnosis`
--
ALTER TABLE `cp_diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_ecg`
--
ALTER TABLE `cp_ecg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_echo`
--
ALTER TABLE `cp_echo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_labs`
--
ALTER TABLE `cp_labs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_medication`
--
ALTER TABLE `cp_medication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_procedure`
--
ALTER TABLE `cp_procedure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
