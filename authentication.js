/*
 * @Author: VINAY SINGH BAGHEL 
 * @Date: 2020-10-21 07:30:34 
 * @Last Modified by: VINAY SINGH BAGHEL 
 * @Last Modified time: 2020-10-21 07:30:34 
 */
const jwt = require('jsonwebtoken');
const config = require('./config');
const db = require('./dbConnection');

function checkForUser(user, callback) {
    db.query('SELECT * FROM user WHERE uId="' + user + '" AND status = 1', function(error, user) {
        if (err) {
            callback(err, null);
        } else if (!user) {
            callback(null, "null");
        } else {
            callback(null, user);
        }
    })
}

module.exports = {

    authenticateJWT: function(req, callback) {
        try {
            let JWTtoken = req.get('Authorization');
            let user = jwt.verify(JWTtoken, jwt_secret);
            checkForUser(user.username, function(err, user) {
                if (err) {
                    callback(err);
                } else if (!user) {
                    callback('unauthorized');
                } else {
                    req.user = user;
                    callback(null, user);
                }
            })
        } catch (err) {
            console.log("error is :%s", err.toString());
            callback('unauthorized');
        }
    },


    validateToken: (req, res, next) => {
        const authorizationHeaader = req.headers.authorization;
        let result;
        if (authorizationHeaader) {
            const token = req.headers.authorization.split(' ')[1]; // Bearer <token>
            // const options = {
            //     subject: 'STlogin',
            //     // expiresIn: 60 * 60 * 11
            // };
            try {
                // verify makes sure that the token hasn't expired and has been issued by us
                result = jwt.verify(token, config.jwt_secret);
                // Let's pass back the decoded token to the request object
                req.decoded = result;
                // We call next to pass execution to the subsequent middleware
                next();
            } catch (err) {
                // Throw an error just in case anything goes wrong with verification
               // throw new Error(err);
               result = {
                error: err,
                status: false
            };
            res.status(401).send(result);
            }
        } else {
            result = {
                error: `Authentication error. Token required.`,
                status: false
            };
            res.status(401).send(result);
        }
    },

    checkReqSession: function(req, res, callback) {
        try {
            let JWTtoken = req.session.token;
            if (JWTtoken) {
                let user = jwt.verify(JWTtoken, jwt_secret);
                checkForUser(user.username, function(err, user) {
                    if (err) {
                        callback(err, null);
                    } else if (!user) {
                        callback(null, 'unauthorized');
                    } else {
                        callback(null, user);
                    }
                    // next();
                })
            } else {

                callback(null, 'unauthorize');
            }
        } catch (error) {
            console.log("Error is checkReqSession", error);
            // res.redirect('/');
            req.session.destroy((err) => {
                if (err) {
                    return console.log(err);
                }
                res.redirect('/');
            });

        }

    }
}