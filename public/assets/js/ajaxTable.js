// ajaxTable.js
var updationArray = [];
function DrawCustomDataTable(tableId, tableHeight, apiUrl, dataColumns, sort_order, form_search_query, filter_column, bFilter, bColumnFilter, headersData, rowId, columnNumber, tableButtons, token, rowCallbackData, drawCallbackData, extraHeaderInfo, requestType, printTitle, customLimit, initialiazationComplete) {
    var selected_column = dataColumns;
    if (tableHeight == 350) {
        tableHeight = 450;
    }
    if (filter_column) {
        selected_column = [];
        _.map(dataColumns, function (obj) {
            if (obj.visible == false || filter_column.indexOf(obj.data) != -1) {
                selected_column.push(obj);
            }
        });
    }
    if(sort_order){
        var ajax = {
            "type": requestType ? requestType : "POST",
            "url": apiUrl,
            
            "data": function (d) {
                d.table_format = "datatable";
                d.soring = d.order;
                delete d.order;
                if(d.soring && d.soring[0]){
                    d.sort_by = d.columns[d.soring[0]['column']]['data'];
                    d.order = d.soring[0]['dir'];
                }else{
                    d.sort_by = "";
                    d.order = "desc";
                }
                if (d.search['value']) {
                    d.search_by = d.search['value'];
                }
                if (form_search_query != "" && $("#" + form_search_query).length) {
                    d.search_query = $("#" + form_search_query).serializeObject();
                }
                d.limit = customLimit ? customLimit : d.length;
            },
            "dataSrc": function (json) {
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                if (json && (typeof (json.voucherTotalArray) === "object")) {
                    updationArray.push({ voucherTotalArray: json.voucherTotalArray });
                    updationArray.push({ DMCTotalArray: json.DMCTotalArray });
                    updationArray.push({ cashInHandTotalArray: json.cashInHandTotalArray });
                    updationArray.push({ actualBudgetArray: json.actualBudgetArray });
                    json.updationArray = json.updationArray;
                }
                if (extraHeaderInfo) {
                    if (json.count > 0) {
                        $("#extraHeaderInfo").html('<span class="ind-net-sal">Net Salary: ' + indianFormatNumber(json.totalNetSalary) + '</span><span class="ind-sal-diff"> Difference: ' + indianFormatNumber(json.totalDifference) + '</span><span class="ind-sal-avg">Avg. Increment: ' + (json.avgIncrement != null ? json.avgIncrement : 0).toFixed(2) + '%</span>');
                    }
                }
                return json.result;
            }
        };
    }else{
        var ajax = {
            "type": requestType ? requestType : "POST",
            "url": apiUrl,
            
            "data": function (d) {
                d.table_format = "datatable";
                if (d.search['value']) {
                    d.search_by = d.search['value'];
                }
                if (form_search_query != "" && $("#" + form_search_query).length) {
                    d.search_query = $("#" + form_search_query).serializeObject();
                }
                d.limit = customLimit ? customLimit : d.length;
            },
            "dataSrc": function (json) {
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
    
                if (json && (typeof (json.voucherTotalArray) === "object")) {
                    updationArray.push({ voucherTotalArray: json.voucherTotalArray });
                    updationArray.push({ DMCTotalArray: json.DMCTotalArray });
                    updationArray.push({ cashInHandTotalArray: json.cashInHandTotalArray });
                    updationArray.push({ actualBudgetArray: json.actualBudgetArray });
                    json.updationArray = json.updationArray;
                }
                if (extraHeaderInfo) {
                    if (json.count > 0) {
                        $("#extraHeaderInfo").html('<span class="ind-net-sal">Net Salary: ' + indianFormatNumber(json.totalNetSalary) + '</span><span class="ind-sal-diff"> Difference: ' + indianFormatNumber(json.totalDifference) + '</span><span class="ind-sal-avg">Avg. Increment: ' + (json.avgIncrement != null ? json.avgIncrement : 0).toFixed(2) + '%</span>');
                    }
                }
                return json.result;
            }
        };
    }
    
    if (token) {
        ajax.beforeSend = function (xhr) {
            xhr.setRequestHeader("Authorization", "JWT " + token);
        };

    }
    var settings = {
        //"bPaginate": false,
        "searching": true,
        //"fixedHeader": true,
        bFilter: bFilter,
        "pageLength": 20,
        "scrollY": tableHeight,
        "scrollX": true,
        processing: true,
        serverSide: true,
        deferRender: true,
        language: {
            processing: "<div class='datatable-loading-inner'><span class='datatable-loading-inner-span'><i class='fa fa-spinner fa-spin' aria-hidden='true'></i>Loading...</span></div>",
        },
        scroller: true,
        "columns": selected_column,
        order: sort_order,
        "bSort": false,
        "ajax": ajax,
        "headerCallback": function (thead, data, start, end, display) {
            // $(thead).find('th').eq(0).html( 'Displaying '+(end-start)+' records' );
            if (headersData) {
                headersData(this, thead, data, start, end, display);
            }

        },
        "rowCallback": function (row, data, index) {
            if (rowCallbackData) {
                // alert(row);
                rowCallbackData(this, row, data, index);
            }
        },
        "drawCallback": function (settings) {

            var api = this.api();

            var rows = api.rows({ page: 'current' }).nodes();

            var last = null;

            api.column(columnNumber, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        // '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                        // '<tr></tr>'

                    );

                    last = group;
                }
            });
            if (drawCallbackData) {
                drawCallbackData(this, api.row().data());
            }

        },
        "initComplete": function (settings, json) {
            if (initialiazationComplete && typeof initialiazationComplete === "function") {
                initialiazationComplete()
            }
        }
        //       //"createdRow": function ( row, data, index ) {
        //}
    }
    if (tableButtons) {
        settings["dom"] = 'Bfrtip';
        settings["buttons"] = [
            { extend: 'copy', footer: true },
            { extend: 'csv', footer: true },
            {
                extend: 'excel',
                // exportOptions: {
                //     columns: ':visible'
                // },
                exportOptions: {
                    modifier: {
                        order: 'index', // 'current', 'applied', 'index',  'original'
                        page: 'all', // 'all',     'current'
                        search: 'none' // 'none',    'applied', 'removed'
                    },
                    columns: ':visible'
                },
                footer: true
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                },
                footer: true,
                title: printTitle,

                customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '8pt');

                    $(win.document.body).find('table')
                        .addClass('compact');

                    $(win.document.body).find('table td')
                        .css('font-size', 'inherit')
                        .css('word-break', 'break-all')
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: ':visible'
                },
                orientation: tableButtons.pdf ? (tableButtons.pdf.orientation ? tableButtons.pdf.orientation : 'portrait') : 'portrait',
                footer: true,
                customize: function (doc) {
                    $('#' + tableId)
                        .addClass('compact')
                        .css('font-size', '8pt')
                        .css('word-break', 'break-all')
                }
            }
        ];
    }
    if (rowId) {
        settings.rowId = rowId;
    }
    // "bSort" : false
    if(sort_order && sort_order!=false){

    }else{
        settings["bSort"] = false;
    }
    // settings["bSort"] = false;
    // console.log("settings",settings);
    customDataTable = $('#' + tableId)
        .DataTable(settings).on('init.dt', function () {
            $("#" + tableId + "_length").hide();
            $("#" + tableId + "_paginate").hide();
        });
    return customDataTable;
}
